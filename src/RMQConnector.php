<?php
namespace Aboutgoods\RabbitMQ;

use Aboutgoods\RabbitMQ\Exceptions\ClientNotConnectedException;
use Aboutgoods\RabbitMQ\Exceptions\InvalidObjectException;
use Aboutgoods\RabbitMQ\Tools\JsonSerializer;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validation;
use Doctrine\Common\Annotations\AnnotationRegistry;

class RMQConnector
{
    private static $client = null;

    public static function connect($config = [])
    {
        if (self::$client == null) {
            self::$client = new Client(
                $config["address"] ?? "rabbitmq://guest:guest@127.0.0.1:5672/jakku"
            );
            self::$client->connect();
            self::$client->connectToChannel($config["passive"] ?? false, $config["durable"] ?? true, $config["exclusive"] ?? false,
                $config["auto_delete"] ?? false);        }

        return self::class;
    }

    public static function setClient(?Client $client)
    {
        self::$client = $client;
    }

    protected static function getClient(): Client
    {
        if (self::$client == null) {
            throw new ClientNotConnectedException();
        }

        return self::$client;
    }

    public static function emit($dataBag)
    {
        if ( ! class_exists('Doctrine\Common\Annotations\AnnotationRegistry', false)
            && class_exists('Doctrine\Common\Annotations\AnnotationRegistry')
        ) {
            if (method_exists('Doctrine\Common\Annotations\AnnotationRegistry', 'registerUniqueLoader')) {
                AnnotationRegistry::registerUniqueLoader('class_exists');
            } else {
                AnnotationRegistry::registerLoader('class_exists');
            }
        }
        $validator = Validation::createValidatorBuilder()->enableAnnotationMapping()->getValidator();
        $violations = $validator->validate($dataBag);
        if (0 !== $violations->count()) {
            throw new InvalidObjectException($violations);
        }
        $content = JsonSerializer::stringify($dataBag);
        self::getClient()->send($content);

        return self::class;
    }
}