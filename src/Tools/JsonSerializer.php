<?php

namespace Aboutgoods\RabbitMQ\Tools;

use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class JsonSerializer
{
    private static $serializer = null;

    public static function getInstance()
    {
        if (self::$serializer === null) {
            $encoders = [new JsonEncoder()];
            $normalizers = [
                new \Symfony\Component\Serializer\Normalizer\DateTimeNormalizer(\DateTime::ISO8601),
                new \Symfony\Component\Serializer\Normalizer\JsonSerializableNormalizer(),
                new ObjectNormalizer(),
            ];
            self::$serializer = new Serializer($normalizers, $encoders);
        }

        return self::$serializer;
    }

    /**
     * @param $object
     *
     * @return bool|float|int|string
     */
    public static function stringify($object)
    {
        $array = self::getInstance()->normalize($object);
        $arrayfiltered = self::array_filter_recursive($array, function($v){return $v !== null;});
        $jsonEncoder = new JsonEncoder();
        $object = $jsonEncoder->encode($arrayfiltered, 'json');
        return $object;
    }

    public static function array_filter_recursive($input, $callback = null)
    {
        foreach ($input as &$value)
        {
            if (is_array($value))
            {
                $value = self::array_filter_recursive($value, $callback);
            }
        }

        return array_filter($input, $callback);
    }

}