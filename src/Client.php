<?php

namespace Aboutgoods\RabbitMQ;

use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Aboutgoods\Url\Url;

class Client
{
    /**
     * @var AMQPStreamConnection
     */
    private $connection;
    /**
     * @var AMQPChannel
     */
    private $channel;
    private $channelName;
    private $address;
    private $port;
    private $login;
    private $password;

    public function __construct($address = 'rabbitmq://guest:guest@localhost:5672/jakku')
    {
        $url = Url::fromString($address);
        if (filter_var($address, FILTER_VALIDATE_URL))
        {
            $userInfos = explode(':', $url->getUserInfo());
            $this->channelName = ltrim($url->getPath(), '/');
            $this->address = $url->getHost();
            $this->port = $url->getPort();
            $this->login = $userInfos[0];
            $this->password = $userInfos[1];
        }
    }

    public function connect(): void
    {
        $this->connection = new AMQPStreamConnection($this->address, $this->port, $this->login, $this->password);
    }

    public function setConnection($connection): void
    {
        if ($connection instanceof AMQPStreamConnection) {
            $this->connection = $connection;
        }
    }

    public function connectToChannel(
        $passive = false,
        $durable = false,
        $exclusive = false,
        $auto_delete = true
    ) {
        $this->channel = $this->connection->channel();
        if(!isset($this->channelName)){
            $this->channelName = 'jakku';
        }
        return $this->channel->queue_declare($this->channelName, $passive, $durable, $exclusive, $auto_delete);
    }

    public function send($content): bool
    {
        $msg = new AMQPMessage($content, ['delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT]);
        $this->channel->basic_publish($msg, '', $this->channelName);
        return true;
    }

    public function __destruct()
    {
        $this->channel->close();
        $this->connection->close();
    }
}