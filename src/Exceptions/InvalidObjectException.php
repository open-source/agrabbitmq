<?php

namespace Aboutgoods\RabbitMQ\Exceptions;

use Symfony\Component\Validator\ConstraintViolationList;
use Throwable;

class InvalidObjectException extends \Exception
{
    /**
     * @var ConstraintViolationList
     */
    private $constraintViolationList;

    public function __construct(ConstraintViolationList $constraintViolationList)
    {
        $this->constraintViolationList = $constraintViolationList;
        parent::__construct("invalid Object for rabbitMQ");
    }

    public function getErrorsAsArray()
    {
        $content = [];
        foreach ($this->constraintViolationList as $violation) {
            $content[] = ["propertyPath" => $violation->getPropertyPath(), "message" => $violation->getMessage()];
        }

        return $content;
    }

    public function getErrorsAsString()
    {
        $message = PHP_EOL;
        foreach ($this->constraintViolationList as $violation) {
            $message .= "\t* ".$violation->getPropertyPath()." - ".$violation->getMessage().PHP_EOL;
        }

        return $message;
    }
}