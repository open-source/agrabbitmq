<?php

namespace Aboutgoods\RabbitMQ\Exceptions;

class ClientNotConnectedException extends \Exception
{
}