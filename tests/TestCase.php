<?php
namespace Test;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;

class TestCase extends \PHPUnit\Framework\TestCase
{
    protected function MockAMQPLib()
    {
        $channel = $this->getMockBuilder(AMQPChannel::class)
            ->disableOriginalConstructor()
            ->setMethods(["close", "basic_publish", "queue_declare"])
            ->getMock()
        ;
        $connector = $this->getMockBuilder(AMQPStreamConnection::class)
            ->disableOriginalConstructor()
            ->setMethods(["channel", "close"])
            ->getMock()
        ;
        $channel->method("basic_publish")->willReturn(null);
        $channel->method("close")->willReturn(true);
        $channel->method("queue_declare")->willReturn(["queue", 30, 0]);
        $connector->method("close")->willReturn(true);
        $connector->method("channel")->willReturn($channel);

        return $connector;
    }
}