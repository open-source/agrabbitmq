<?php
/**
 * Created by PhpStorm.
 * User: henri
 * Date: 7/27/18
 * Time: 9:35 AM
 */
namespace Test;

use Aboutgoods\RabbitMQ\Client;

class ClientTest extends TestCase
{


    public function testSend()
    {
        $client = new Client(null, null, null, null);
        $client->setConnection($this->MockAMQPLib());
        $response = $client->connectToChannel("a", true, true, false, true);
        $sent = $client->send("test");
        $this->assertTrue($sent);
        $this->assertCount(3, $response);
    }
}